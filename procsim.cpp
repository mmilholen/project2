#include "procsim.hpp"
#include <deque>
#include <map>
#include <vector>
#include <stdlib.h>
#include <cstdio>
#include <cinttypes>
#include <limits>

typedef struct _proc_spec_t
{
    uint64_t d;
    uint64_t k0;
    uint64_t k1;
    uint64_t k2;
    uint64_t f;
    uint64_t m;
    uint64_t c;
    uint64_t dispatch_queue_limit;
    uint64_t scheduling_queue_limit[3];
} proc_spec_t;

typedef struct _tag_status_t
{
    bool busy;
    uint32_t tag;
} tag_status_t;

typedef struct _scoreboard_t
{
    bool busy;
} scoreboard_t;

proc_spec_t *proc_spec;
std::deque<proc_inst_t *> dispatch_queue;
std::vector<tag_status_t *> register_file;
uint32_t curr_tag = 1;
std::map<uint32_t, std::deque<proc_inst_t *>> scheduling_queue;
std::vector<cdb_line_t *> cdb; // common data bus
const uint64_t fu_cycles[3] = {1, 3, 3};
scoreboard_t * scoreboard[3];

// stall identification
bool fetch_stall = false;
bool dispatch_stall = false;
bool schedule_stall = false;
bool execute_stall = false;
bool status_stall = false;

bool dispatch_queue_is_full() {
    return dispatch_queue.size() >= proc_spec->dispatch_queue_limit;
}

int32_t fu_for_op_code(int32_t op_code) {
    return op_code == FU_NONE ? 0 : op_code;
}

bool scheduling_queue_is_full(uint32_t fu) {
    return scheduling_queue[fu].size() >= proc_spec->scheduling_queue_limit[fu];
}

cdb_line_t * first_avail_cdb_line() {
    cdb_line_t *cdb_line = NULL;
    for (std::vector<cdb_line_t *>::iterator it = cdb.begin(); it != cdb.end(); it++) {
        cdb_line_t *curr = *it;
        if (!curr->busy) {
            cdb_line = curr;
            break;
        }
    }
    return cdb_line;
}

void print_inst_stats(proc_inst_t *inst) {
    printf("%lu %lu %lu %lu %lu %lu\n", inst->instruction_id, inst->fetch_cycle, inst->dispatch_cycle, inst->schedule_cycle, inst->execute_cycle, inst->status_cycle);
}

void print_inst_detail(proc_inst_t *inst) {
    printf("%lu %ld %ld %ld %ld %" PRIu32 " %" PRIu32 " %d %" PRIu32 " %d %" PRIu64 "\n", inst->instruction_id, (long)inst->op_code, (long)inst->dest_reg, (long)inst->src_reg0, (long)inst->src_reg1, inst->dest_tag, inst->src_tag0, inst->src_ready0, inst->src_tag1, inst->src_ready1, inst->fu_cycles_completed);
}

void print_full_pipeline() {
    printf("\nCOMMON DATA BUS\n");
    for (uint64_t i = 0; i < proc_spec->c; i++) {
        cdb_line_t *line = cdb[i];
        printf("%" PRIu64 ": %" PRIu32 " %ld %d\n", i, line->tag, (long)line->reg, line->busy);
    }

    printf("\nREGISTER FILE\n");
    int reg = 0;
    for (std::vector<tag_status_t *>::iterator it = register_file.begin(); it != register_file.end(); it++) {
        tag_status_t *file = *it;
        printf("%d %" PRIu32 " %d\n", reg++, file->tag, file->busy);
    }

    printf("\nDISPATCH QUEUE\n");
    for (std::deque<proc_inst_t *>::iterator it = dispatch_queue.begin(); it != dispatch_queue.end(); it++) {
        proc_inst_t *inst = *it;
        printf("%lu %ld %ld %ld %ld\n", inst->instruction_id, (long)inst->op_code, (long)inst->dest_reg, (long)inst->src_reg0, (long)inst->src_reg1);
    }

    printf("\nSCHEDULING QUEUE\n");
    for (int32_t i = 0; i < 3; i++) {
        for (std::deque<proc_inst_t *>::iterator it = dispatch_queue.begin(); it != dispatch_queue.end(); it++) {
            proc_inst_t *inst = *it;
            print_inst_detail(inst);
        }
    }
}

/**
 * Subroutine for initializing the processor. You many add and initialize any global or heap
 * variables as needed.
 * XXX: You're responsible for completing this routine
 *
 * @d Dispatch queue multiplier
 * @k0 Number of k0 FUs
 * @k1 Number of k1 FUs
 * @k2 Number of k2 FUs
 * @f Number of instructions to fetch
 * @m Schedule queue multiplier
 * @c Number of CDBs
 */
void setup_proc(uint64_t d, uint64_t k0, uint64_t k1, uint64_t k2, uint64_t f, uint64_t m, uint64_t c) {
    proc_spec = new proc_spec_t();
    proc_spec->d = d;
    proc_spec->k0 = k0;
    proc_spec->k1 = k1;
    proc_spec->k2 = k2;
    proc_spec->f = f;
    proc_spec->m = m;
    proc_spec->c = c;

    // init dispatch queue
    proc_spec->dispatch_queue_limit = d * m *(k0 + k1 + k2);

    // init register file
    register_file.resize(8);
    for (int32_t i = 0; i < 8; i++) {
        tag_status_t *tag_status = new tag_status_t();
        tag_status->tag = TAG_UNASSIGNED;
        tag_status->busy = false;
        register_file[i] = tag_status;
    }

    // init scheduling queue
    proc_spec->scheduling_queue_limit[0] = m * k0;
    proc_spec->scheduling_queue_limit[1] = m * k1;
    proc_spec->scheduling_queue_limit[2] = m * k2;

    scoreboard[0] = new scoreboard_t();
    scoreboard[0]->busy = false;
    scoreboard[1] = new scoreboard_t();
    scoreboard[1]->busy = false;
    scoreboard[2] = new scoreboard_t();
    scoreboard[2]->busy = false;

    // init cdb
    cdb.resize(c);
    for (uint64_t i = 0; i < c; i++) {
        cdb_line_t *cdb_line = new cdb_line_t();
        cdb_line->busy = false;
        cdb_line->tag = TAG_UNASSIGNED;
        cdb_line->reg = REG_NONE;
        cdb[i] = cdb_line;
    }
}

void perform_status_update_register(proc_stats_t* p_stats) {
    bool stall = true;
    for (std::vector<cdb_line_t *>::iterator it = cdb.begin(); it != cdb.end(); it++) {
        cdb_line_t *line = *it;
        if (line == NULL) {
            break;
        }

        if (line->busy) {
            line->busy = false;
            if (line->reg != REG_NONE && register_file[line->reg]->tag == line->tag) {
                register_file[line->reg]->busy = false;
            }
            stall = false;
            p_stats->retired_instruction++;
        }
    }

    status_stall = stall;
}

void perform_status_update_deletions(proc_stats_t* p_stats) {
    for (int fu = 0; fu < 3; fu++) {
        int elements_to_remove = 0;
        for (std::deque<proc_inst_t *>::iterator it = scheduling_queue[fu].begin(); it != scheduling_queue[fu].end(); it++) {
            proc_inst_t *inst = *it;
            if (!inst->done_executing) {
                break;
            }
            inst->status_cycle = p_stats->cycle_count;

            print_inst_stats(inst);
            delete inst;
            elements_to_remove++;
        }

        // never manipulate an iterable while you're iterating
        if (elements_to_remove > 0) {
            scheduling_queue[fu].erase(scheduling_queue[fu].begin(), scheduling_queue[fu].begin() + elements_to_remove);
        }
    }
}

bool perform_execute(proc_stats_t* p_stats) {
    bool stall = true;
    for (int fu = 0; fu < 3; fu++) {
        for (std::deque<proc_inst_t *>::iterator it = scheduling_queue[fu].begin(); it != scheduling_queue[fu].end(); it++) {
            proc_inst_t *inst = *it;
            if (inst == NULL || !inst->allowed_to_execute) {
                break;
            }

            // execute instruction
            if (inst->fu_cycles_completed >= fu_cycles[fu]) {
                inst->done_executing = true;
                cdb_line_t *cdb_line = first_avail_cdb_line();
                if (cdb_line != NULL) {
                    cdb_line->busy = true;
                    cdb_line->tag = inst->dest_tag;
                    cdb_line->reg = inst->dest_reg;
                }
            } else if (!inst->done_executing) {
                inst->fu_cycles_completed++;
                scoreboard[fu]->busy = false;
            }
            stall = false;
        }
    }

    execute_stall = stall;
    return !scheduling_queue[0].empty() || !scheduling_queue[1].empty() || !scheduling_queue[2].empty(); // work remaining
}

bool perform_schedule_reserve(proc_stats_t* p_stats) {
    bool stall = true;
    bool work_remains = false;
    for (int fu = 0; fu < 3; fu++) {
        for (std::deque<proc_inst_t *>::iterator it = scheduling_queue[fu].begin(); it != scheduling_queue[fu].end(); it++) {
            proc_inst_t *inst = *it;
            if (inst == NULL || inst->fu_cycles_completed > 0) {
                continue;
            }

            // reserve fu
            if (inst->src_ready0 && inst->src_ready1 && !scoreboard[fu]->busy) {
                scoreboard[fu]->busy = true;
                inst->allowed_to_execute = true;
                inst->execute_cycle = p_stats->cycle_count + 1;
                stall = false;
            } else {
                work_remains = true;
            }
        }
    }

    schedule_stall = stall;
    return work_remains;
}

void perform_schedule_update(proc_stats_t* p_stats) {
    for (int fu = 0; fu < 3; fu++) {
        for (std::deque<proc_inst_t *>::iterator it = scheduling_queue[fu].begin(); it != scheduling_queue[fu].end(); it++) {
            proc_inst_t *inst = *it;
            if (inst == NULL || inst->fu_cycles_completed > 0) {
                continue;
            }

            // update ready bits
            for (std::vector<cdb_line_t *>::iterator line = cdb.begin(); line != cdb.end(); line++) {
                cdb_line_t *curr_line = *line;
                if (!curr_line->busy) {
                    continue;
                }

                if (inst->src_tag0 != TAG_UNASSIGNED && inst->src_tag0 == curr_line->tag) {
                    inst->src_ready0 = true;
                }
                if (inst->src_tag1 != TAG_UNASSIGNED && inst->src_tag1 == curr_line->tag) {
                    inst->src_ready1 = true;
                }
            }
        }
    }
}

bool perform_dispatch_reserve(proc_stats_t* p_stats) {
    int dispatch_elements_to_remove = 0;
    bool stall = true;
    for (std::deque<proc_inst_t *>::iterator it = dispatch_queue.begin(); it != dispatch_queue.end(); it++) {
        proc_inst_t *inst = *it;
        uint32_t fu = fu_for_op_code(inst->op_code);
        if (scheduling_queue_is_full(fu)) {
            break;
        }

        // allocate new to prevent memory leak
        proc_inst_t *pushable = new proc_inst_t();
        pushable->op_code = inst->op_code;
        pushable->src_reg0 = inst->src_reg0;
        pushable->src_ready0 = true;
        pushable->src_tag0 = TAG_UNASSIGNED;
        pushable->src_reg1 = inst->src_reg1;
        pushable->src_ready1 = true;
        pushable->src_tag1 = TAG_UNASSIGNED;
        pushable->dest_reg = inst->dest_reg;
        pushable->allowed_to_execute = false;
        pushable->fu_cycles_completed = 0;
        pushable->done_executing = false;
        pushable->instruction_id = inst->instruction_id;
        pushable->instruction_address = inst->instruction_address;
        pushable->fetch_cycle = inst->fetch_cycle;
        pushable->dispatch_cycle = inst->dispatch_cycle;
        pushable->schedule_cycle = p_stats->cycle_count + 1;
        pushable->execute_cycle = inst->execute_cycle;
        pushable->status_cycle = inst->status_cycle;

        // set src ready/tag for inst
        if (inst->src_reg0 != REG_NONE) {
            pushable->src_ready0 = !register_file[inst->src_reg0]->busy;
            pushable->src_tag0 = register_file[inst->src_reg0]->tag;
        }

        if (inst->src_reg1 != REG_NONE) {
            pushable->src_ready1 = !register_file[inst->src_reg1]->busy;
            pushable->src_tag1 = register_file[inst->src_reg1]->tag;
        }

        // update register file
        if (inst->dest_reg != REG_NONE) {
            register_file[inst->dest_reg]->tag = curr_tag++;
            register_file[inst->dest_reg]->busy = true;
            pushable->dest_tag = register_file[inst->dest_reg]->tag;
        }

        delete inst;
        scheduling_queue[fu].push_back(pushable);

        stall = false;
        dispatch_elements_to_remove++;
    }

    // never manipulate an iterable while you're iterating
    if (dispatch_elements_to_remove > 0) {
        dispatch_queue.erase(dispatch_queue.begin(), dispatch_queue.begin() + dispatch_elements_to_remove);
    }

    dispatch_stall = stall;
    return !dispatch_queue.empty();
}

void perform_dispatch_update(proc_stats_t* p_stats) {
    for (std::deque<proc_inst_t *>::iterator it = dispatch_queue.begin(); it != dispatch_queue.end(); it++) {
        proc_inst_t *inst = *it;

        // update ready bits
        if (inst->src_reg0 != REG_NONE && !register_file[inst->src_reg0]->busy) {
            inst->src_ready0 = true;
        }
        if (inst->src_reg1 != REG_NONE && !register_file[inst->src_reg1]->busy) {
            inst->src_ready1 = true;
        }
        if (inst->dest_reg != REG_NONE && !register_file[inst->dest_reg]->busy) {
            inst->dest_reg = true;
        }
    }
}

bool perform_fetch(proc_stats_t* p_stats) {
    uint64_t inst_fetched = 0;
    bool work_remaining = true;
    bool stall = true;
    while (!dispatch_queue_is_full() && inst_fetched < proc_spec->f) {
        proc_inst_t *inst = new proc_inst_t();

        if (read_instruction(inst)) {
            inst->src_ready0 = false;
            inst->src_tag0 = TAG_UNASSIGNED;
            inst->src_ready1 = false;
            inst->src_tag1 = TAG_UNASSIGNED;
            inst->dest_tag = TAG_UNASSIGNED;
            inst->allowed_to_execute = false;
            inst->fu_cycles_completed = 0;
            inst->instruction_id = ++p_stats->fired_instruction;
            inst->fetch_cycle = p_stats->cycle_count;
            inst->dispatch_cycle = p_stats->cycle_count + 1;
            dispatch_queue.push_back(inst);

            inst_fetched++;
        } else { // no more instructions to fetch
            delete inst;
            work_remaining = false;
            break;
        }

        stall = false;
    }

    fetch_stall = stall;
    return work_remaining || inst_fetched > 0;
}

/**
 * Subroutine that simulates the processor.
 *   The processor should fetch instructions as appropriate, until all instructions have executed
 * XXX: You're responsible for completing this routine
 *
 * @p_stats Pointer to the statistics structure
 */
void run_proc(proc_stats_t* p_stats) {
    printf("INST    FETCH    DISP    SCHED    EXEC    STATE\n");

    uint64_t max_iters = std::numeric_limits<uint64_t>::max();
    bool work_remaining = true;
    int consecutive_stalls = 0;
    while (work_remaining && p_stats->cycle_count < max_iters) {
        // first half
        perform_status_update_register(p_stats);
        work_remaining = perform_execute(p_stats);
        work_remaining |= perform_schedule_reserve(p_stats);
        work_remaining |= perform_dispatch_reserve(p_stats);
        work_remaining |= perform_fetch(p_stats);

        // second half
        perform_status_update_deletions(p_stats);
        perform_schedule_update(p_stats);
        perform_dispatch_update(p_stats);

        // safeguard
        if (work_remaining && fetch_stall && dispatch_stall && schedule_stall && execute_stall && status_stall) {
            consecutive_stalls++;
        } else {
            consecutive_stalls = 0;
        }

        if (consecutive_stalls >= 5) {
            printf("operation stalled at iteration %" PRIu64 "\n", p_stats->cycle_count);
            print_full_pipeline();
            break;
        }

        p_stats->cycle_count++;
    }
}

/**
 * Subroutine for cleaning up any outstanding instructions and calculating overall statistics
 * such as average IPC or branch prediction percentage
 * XXX: You're responsible for completing this routine
 *
 * @p_stats Pointer to the statistics structure
 */
void complete_proc(proc_stats_t *p_stats) {
    // update status
    p_stats->avg_inst_fire = p_stats->cycle_count > 0 ? ((float)p_stats->retired_instruction / (float)p_stats->cycle_count) : 0;

    // free resources
    delete proc_spec;
    for (std::vector<tag_status_t *>::iterator it = register_file.begin(); it != register_file.end(); it++) {
        delete *it;
    }
    for (int i = 0; i < 3; i++) {
        delete scoreboard[i];
    }
    for (std::vector<cdb_line_t *>::iterator it = cdb.begin(); it != cdb.end(); it++) {
        delete *it;
    }
}

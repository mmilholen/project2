#ifndef PROCSIM_HPP
#define PROCSIM_HPP

#include <cstdint>

#define DEFAULT_K0 1
#define DEFAULT_K1 2
#define DEFAULT_K2 3
#define DEFAULT_D 2
#define DEFAULT_M 2
#define DEFAULT_F 4
#define DEFAULT_C 4

#define TAG_UNASSIGNED 0
#define FU_NONE -1
#define REG_NONE -1

typedef struct _proc_inst_t
{
    uint32_t instruction_address;
    int32_t op_code;
    int32_t src_reg0;
    int32_t src_reg1;
    int32_t dest_reg;
    
    // You may introduce other fields as needed
    uint32_t dest_tag;
    bool src_ready0;
    bool src_ready1;
    uint32_t src_tag0;
    uint32_t src_tag1;
    bool allowed_to_execute;
    uint64_t fu_cycles_completed;
    bool done_executing;

    // statistic info
    unsigned long instruction_id;
    unsigned long fetch_cycle;
    unsigned long dispatch_cycle;
    unsigned long schedule_cycle;
    unsigned long execute_cycle;
    unsigned long status_cycle;
} proc_inst_t;

typedef struct _proc_stats_t
{
    float avg_inst_fire;
    unsigned long fired_instruction;
    unsigned long retired_instruction;
    unsigned long cycle_count;
    float avg_ipc;
    float perc_branch_pred;
} proc_stats_t;

typedef struct _cdb_line_t
{
    bool busy;
    uint32_t tag;
    int32_t reg;
} cdb_line_t;

bool read_instruction(proc_inst_t* p_inst);

void setup_proc(uint64_t d, uint64_t k0, uint64_t k1, uint64_t k2, uint64_t f, uint64_t m, uint64_t c);
void run_proc(proc_stats_t* p_stats);
void complete_proc(proc_stats_t* p_stats);

#endif /* PROCSIM_HPP */
